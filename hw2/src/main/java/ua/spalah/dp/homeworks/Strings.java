package ua.spalah.dp.homeworks;

public class Strings {
    public static void main( String[] args )
    {
        if(args[0] == "" || args.length != 1)
        {
            System.err.println("Invalid argument");
            System.exit(1);
        }
        String stroka = args[0];
        System.out.println("You entered:"+ stroka);
        String copy = "";
        for (int i = 0; i < stroka.length(); i++) {
            if(Character.isDigit(stroka.charAt(i)))
                continue;
            if (stroka.charAt(i) == '+')
                copy = copy + "+";
            if (stroka.charAt(i) == '-')
                copy = copy + "-";
            copy = copy + stroka.charAt(i);
            //System.out.println(stroka.charAt(i));
        }
        copy = copy.trim();
        copy = copy.replace("hello", "HELLO");
        System.out.println("Your string after conversions:"+copy);
    }
}
