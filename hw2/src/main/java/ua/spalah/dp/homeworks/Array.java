package ua.spalah.dp.homeworks;

import java.util.Random;

/**
 * Homework2
 */
public class Array {
    public static void main(String[] args) {
        int min = -50;
        int max = 80;
        int[][] array = new int[8][5];
        fillArray(min, max, array);
        System.out.println("Random array [8][5] was generated:");
        showArray(array);
        System.out.println("Maximum value of an array element is " + findMaxValueOfArray(array));
        System.out.println("Minimum value of an array element is " + findMinValueOfArray(array));
        System.out.println("The index of the raw with the largest multiple of elements is " + findIndexOfRawLargest(array));
        System.out.println("The index of the column with the largest multiple of elements is " + findIndexOfColumnLargest(array));
        for (int i = 0; i < array.length; i++) {
            BubbleSort(array[i]);
        }
        System.out.println("Sorted array:");
        showArray(array);
    }

    private static int findIndexOfColumnLargest(int[][] array) {
        long[] multipleColumn = {1, 1, 1, 1, 1};
        long maxMultipleColumn = 0;
        int maxMultipleColumnId = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                multipleColumn[j] = multipleColumn[j] * Math.abs(array[i][j]);
                if (i == 7) {
                    if (maxMultipleColumn < multipleColumn[j]) {
                        maxMultipleColumn = multipleColumn[j];
                        maxMultipleColumnId = j;
                    }
                }
            }

        }
        return maxMultipleColumnId;
    }

    private static int findIndexOfRawLargest(int[][] array) {
        int[] multipleRaw = {1, 1, 1, 1, 1, 1, 1, 1};
        int maxMultipleRaw = 0;
        int maxMultipleRawId = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                multipleRaw[i] = multipleRaw[i] * Math.abs(array[i][j]);
            }
            if (maxMultipleRaw < multipleRaw[i]) {
                maxMultipleRaw = multipleRaw[i];
                maxMultipleRawId = i;
            }
        }
        return maxMultipleRawId;
    }

    private static int findMinValueOfArray(int[][] array) {
        int min_value = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (min_value >= array[i][j])
                    min_value = array[i][j];
            }
        }
        return min_value;
    }

    private static int findMaxValueOfArray(int[][] array) {
        int max_value = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (max_value <= array[i][j])
                    max_value = array[i][j];
            }
        }
        return max_value;
    }

    private static void showArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(String.format("|%4d| ", array[i][j]));
            }
            System.out.println();
        }
    }

    private static void fillArray(int min, int max, int[][] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(max - min + 1) + min;
            }
        }
    }

    private static void BubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 1; j < (array.length - i); j++) {
                if (array[j - 1] > array[j]) {
                    int tmp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = tmp;
                }
            }
        }
    }
}
