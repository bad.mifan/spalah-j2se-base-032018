package ua.spalah.dp.homeworks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.SQLOutput;
import java.util.Arrays;

/**
 * Homework1!
 *
 */
public class App {
    public static void main(String[] args) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                System.out.println("Enter something : ");
                String input = br.readLine();
                if ("q".equals(input)) {
                    System.out.println("Quit!");
                    System.exit(0);
                }
                String[] arr = input.split("\\+|\\-|\\*|\\/");
                //System.out.println("tmp : " + Arrays.deepToString(arr));
                BigDecimal result = new BigDecimal(arr[0]);
                //System.out.println(result);

                for (int i = 0, index = 0; i < input.length(); i++) {
                    if (!Character.isDigit(input.charAt(i))) {
                        if (input.charAt(i) == '+') {
                            index++;
                            BigDecimal tmp = new BigDecimal(arr[index]);
                            //System.out.println(tmp);
                            result = result.add(tmp);
                        } else if (input.charAt(i) == '-') {
                            index++;
                            BigDecimal tmp = new BigDecimal(arr[index]);
                            result = result.subtract(tmp);
                        } else if (input.charAt(i) == '*' || input.charAt(i) == '/'){
                            System.out.println(ErrorMessages.Errors[2]);
                            System.exit(0);
                        }
                    }
                }
                System.out.println("input : " + input + " = " + result);
                //System.out.println("tmp : " + Arrays.deepToString(arr));
                System.out.println("-----------\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}