package ua.spalah.dp.homeworks;

public interface FindElement {
    int find(int[] arr, int indexTo);
}