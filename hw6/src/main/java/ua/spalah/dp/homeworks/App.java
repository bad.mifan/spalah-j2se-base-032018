package ua.spalah.dp.homeworks;

public class App {
    public static void main(String[] args) {
        Sort bubleSortAsc = new BubleSortAsc();
        Sort bubleSortDesc = new BubleSortDesc();
        Sort dummySort = new DummySort();
        FindElement findMinElement = new FindMinElement();

        sort(bubleSortAsc);
        sort(bubleSortDesc);
        sort(dummySort);
        find(findMinElement);
    }

    private static void find(FindElement findElement) {
        if (findElement instanceof FindMinElement) {
            System.out.println("call FindMinElement");
        } else if (findElement instanceof FindMaxElement) {
            System.out.println("call FindMaxElement");
        } else {
            System.out.println("call " + findElement.getClass().getName());
        }

        int maxLength = 5;
        BlackBoard blackBoard1 = new BlackBoard(findElement, maxLength);
        blackBoard1.add(200);
        blackBoard1.add(100);
        blackBoard1.add(300);
        blackBoard1.draw();
    }

    private static void sort(Sort sort) {
        if (sort instanceof BubleSortAsc) {
            System.out.println("call BubleSortAsc");
        } else if (sort instanceof BubleSortDesc) {
            System.out.println("call BubleSortDesc");
        } else {
            System.out.println("call " + sort.getClass().getName());
        }

        int maxLength = 5;
        BlackBoard blackBoard = new BlackBoard(sort, maxLength);
        blackBoard.add(200);
        blackBoard.add(100);
        blackBoard.add(300);
        blackBoard.draw();
    }

}
