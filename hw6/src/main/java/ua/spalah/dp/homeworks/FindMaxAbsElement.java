package ua.spalah.dp.homeworks;

import static java.lang.Math.abs;

public class FindMaxAbsElement extends AbstractFindElement {
    @Override
    protected boolean checkElement(int left, int right) {
        return (abs(left) < abs(right) ? true : false);
    }
}
