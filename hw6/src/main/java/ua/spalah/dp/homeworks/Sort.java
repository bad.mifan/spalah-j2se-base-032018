package ua.spalah.dp.homeworks;

public interface Sort {
    void sort(int[] arr, int indexTo);
}
