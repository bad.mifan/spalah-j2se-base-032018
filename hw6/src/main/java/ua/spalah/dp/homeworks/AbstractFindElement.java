package ua.spalah.dp.homeworks;

public abstract class AbstractFindElement implements FindElement{
    @Override
    public int find(int[] array, int indexTo) {
        int tempValue = 0;
        for (int i = 0; i < indexTo; i++) {
            if(checkElement(tempValue, array[i]))
                tempValue = array[i];
        }
        return tempValue;
    }

    protected abstract boolean checkElement(int left, int right);
}
